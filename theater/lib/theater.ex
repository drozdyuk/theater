# Simulates the aggregates that always succeed
defmodule Theater do
    ## Order Aggregate
    # Place order - provides correlation id here
    def po(date, play_name, num_tickets) do
      order_id =  make_id()
      e = %Theater.Events.OrderCreated{order_id: order_id, num_tickets: num_tickets, 
        date: date, play_name: play_name}
      w = write_event("orders-#{order_id}", e)
      {:ok, _} = Extreme.execute(Theater.EventStore, w)
    end
    # Confirm order
    def co(order_id) do
        e = %Theater.Events.OrderConfirmed{ order_id: order_id }
        w = write_event("orders-#{order_id}", e)
        {:ok, _} = Extreme.execute(Theater.EventStore, w)
    end
    # Reservation Aggregate
    # Make reservation
    def mr(order_id, num_seats, date) do
        reservation_id = make_id() 
        e = %Theater.Events.SeatsReserved{
              reservation_id: reservation_id, 
              order_id: order_id,
              seats: Enum.to_list(3..num_seats+3), # some random seat arrangement
              date: date
            }
        w = write_event("reservations-#{reservation_id}", e)
        {:ok, _} = Extreme.execute(Theater.EventStore, w)
    end

    # Payment Aggregate
    # Make payment
    def mp(reservation_id, amount) do
        invoice_id = make_id()
        e = %Theater.Events.PaymentAccepted{
          reservation_id: reservation_id,
          amount: amount,
          invoice_id: invoice_id
        }
        w = write_event("payments-#{invoice_id}", e)
        {:ok, _} = Extreme.execute(Theater.EventStore, w)
    end

    # alias Extreme.Messages, as: ExMsg
    defp write_event(stream, event) do
        proto_event = Extreme.Messages.NewEvent.new(
            event_id: Extreme.Tools.gen_uuid(),
            event_type: to_string(event.__struct__),
            data_content_type: 0,
            metadata_content_type: 0,
            data: :erlang.term_to_binary(event),
            metadata: ""
        ) 
        Extreme.Messages.WriteEvents.new(
          event_stream_id: stream, 
          expected_version: -2,
          events: [proto_event],
          require_master: false
        )
    end
    def make_id() do
      Keyword.get(UUID.info!(UUID.uuid1()), :uuid)
    end
end
