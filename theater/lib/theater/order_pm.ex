# Order Process Manager from Microsoft:
# Figure 2:
# https://msdn.microsoft.com/en-us/library/jj591569.aspx
defmodule Theater.OrderPm do
  use GenServer
  # Handles on order at a time! Haha
  defstruct current_order: nil 

  def start_link() do
    IO.puts("STARTED!!")
    GenServer.start_link(__MODULE__, {}, [name: Theater.OrderPm])
  end

  def init({}) do
    {:ok, %Theater.OrderPm{}}
  end
  
  def process_event(event) do
    IO.puts("PROCESSING EVENT...")
    GenServer.call(Theater.OrderPm, {:process_event, event})
  end

  def handle_call({:process_event, event}, _from, state) do
    do_process_event(event, state)
  end

  def do_process_event(%Theater.Events.OrderCreated{order_id: order_id, date: date, num_tickets: num_tickets}, state) do
    Theater.mr(order_id, num_tickets, date)
    {:reply, :ok, %{state|current_order: order_id}}
  end
  def do_process_event(%Theater.Events.SeatsReserved{reservation_id: reservation_id, total_cost: total_cost}, state) do
    Theater.mp(reservation_id, total_cost)
    {:reply, :ok, state}
  end

  def do_process_event(%Theater.Events.PaymentAccepted{}, %{current_order: order_id}=state) do
    Theater.co(order_id)
    {:reply, :ok, state}
  end

  def do_process_event(%Theater.Events.OrderConfirmed{order_id: order_id}, %{current_order: order_id}=state) do
    {:reply, :ok, %{state| current_order: nil}}
  end

  def do_process_event(_, state) do
    IO.puts("Ignoring event.")
    {:reply, :ok, state}
  end


end
