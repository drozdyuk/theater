defmodule Theater.Listener do
    use Extreme.Listener

    defp get_last_event(_stream_name) do
      case File.read("last-event.txt") do
        {:ok, x} -> 
          {last_event, _} = Integer.parse(x)
          last_event
        {:error, _} -> -1
      end
    end

    defp process_push(push, stream_name) do
        IO.puts("Processing push...")
        event_number = push.link.event_number
        event = :erlang.binary_to_term(push.event.data)
        metadata = push.event.metadata
        event_type = push.event.event_type
        Theater.OrderPm.process_event(event)
        {:ok, file} = File.open("last-event.txt", [:write])
        IO.binwrite(file, to_string(event_number))
        {:ok, event_number}
    end

    defp caught_up, do: Logger.debug("We are caught up.")
end
