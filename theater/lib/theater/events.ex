defmodule Theater.Events do
    defmodule OrderCreated do
      defstruct [:order_id, :date, :play_name, :num_tickets]
    end
    defmodule OrderConfirmed do
      defstruct [:order_id]
    end

    defmodule SeatsReserved do
      defstruct [:reservation_id, :order_id, :seats, :date, :total_cost]
    end

    defmodule PaymentAccepted do
      defstruct [:reservation_id, :amount, :invoice_id]
    end
end
