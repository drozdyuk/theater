defmodule Theater.Application do
  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false
  @event_store Theater.EventStore

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    # Define workers and child supervisors to be supervised
    event_store_settings = Application.get_env :theater, :event_store
    children = [
        worker(Extreme, [event_store_settings, [name: @event_store]]),
        worker(Theater.OrderPm, []),
        worker(Theater.Listener, [@event_store, "orders_pm", [Theater.Listener]])
      # Starts a worker by calling: Theater.Worker.start_link(arg1, arg2, arg3)
      # worker(Theater.Worker, [arg1, arg2, arg3]),
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Theater.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
