# Theater #

This project aims to show an example of a process manager based on the "Order Process Manager" sample from microsoft's example: https://msdn.microsoft.com/en-us/library/jj591569.aspx

### Description ###

This projects models a theater, with distict order, reservation and payment stages.

![architecture](architecture.png)


### How do I get set up? ###

1. Prepare environment

        docker-compose up -d
        sh dev.sh
        cd /app/theater
   You can now connect to http://localhost:2113 to view event store and inspect events. 

2. Go to [Projections](http://localhost:2113/web/index.html#/projections) in the evenstore, and create a new projection with any name, and a content from `proj.txt`. Make sure it is set to *Continuous* and *Emit enabled* is checked.

3. Delete `last-event.txt` in root folder (if present). You will need to delete it every time you re-create the eventstore.
4. Issue a *place order* command to get things going, by first starting the shell, and then calling the app:

        iex -S mix
        > Theater.po("Aug 20, 2017", "Taming of the Shrew", 2)

### Event Storming ###

This is the event storming session I had, to come up with events and commands.
As you can see, I needed to introduce a `ConfirmOrder` command, to replace the
`OrderConfirmed` event that order process manager emits in the example.

![event-storming](event-storming.jpg)